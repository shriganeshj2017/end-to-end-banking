package com.endbanking.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheEndToEndBankingApplication {

	public static void main(String[] args) {
		System.out.println();
		SpringApplication.run(TheEndToEndBankingApplication.class, args);
	}

}
